#include <iostream>
#include <conio.h>

class complex {
public:
    complex(double a) : re(a) {};
    complex(double a, double b) : re(a), im(b) {};

    double real() { return re; }
    double imag() { return im; }

    complex operator -() {
        return complex(-re, -im);
    }
    
    complex operator +(const complex rhs) {
        return complex(re + rhs.re, im + rhs.im);
    }

    complex operator -(const complex rhs) {
        return complex(re - rhs.re, im - rhs.im);
    }

    complex operator =(const complex rhs) {
        re = rhs.re;
        im = rhs.im;
        return *this;
    }

    complex operator *(const complex rhs) {
        double real = re * rhs.re - im * rhs.im;
        double imag = re * rhs.im + im * rhs.re;
        return complex(real, imag);
    }

    complex operator /(const complex rhs) {
        double real = (re * rhs.re + im * rhs.im) / (rhs.re * rhs.re + rhs.im * rhs.im);
        double imag = (rhs.re * im - re * rhs.im) / (rhs.re * rhs.re + rhs.im * rhs.im);
        return complex(real, imag);
    }

    complex operator ~() {
        return complex(re, -im);
    }

    double abs() {
        return std::pow(re * re + im * im, 0.5);
    }

private:
    double re = 0;
    double im = 0;
};

std::ostream& operator <<(std::ostream& o, complex c) {
    char* imag;
    (c.imag() >= 0) ? imag = "+" : imag = "-";
    o << c.real() << imag << std::abs(c.imag()) << "i";
    return o;
}

int main() {

    complex a(0, 1), b(2, 0);

    std::cout << "Arithmetics from cppreference.com:" << std::endl;
    std::cout << std::endl;

    std::cout << a << " + " << b << " = " << a + b << std::endl;
    std::cout << std::endl;

    std::cout << a << " * " << a << " = " << a * b << std::endl;
    std::cout << std::endl;

    std::cout << a << " + " << b << " / " << a 
        << " = " << a + b / a << std::endl;
    std::cout << std::endl;

    std::cout << -complex(1) << " / " << a << " = " << -complex(1) / a << std::endl;
    std::cout << std::endl;

    a = complex(-6, 8);
    std::cout << "abs(" << a << ") = " << a.abs() << std::endl;
    std::cout << std::endl;

    std::cout << "~(" << a << ") = " << ~a << std::endl;
    std::cout << std::endl;

    _getch();

    return 0;
}
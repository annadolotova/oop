#include <stdio.h>
#include <iostream>
#include <exception>
#include <conio.h>

enum MType {
	SPARSE,
	COLUMN,
	ROW
};

class Sparse;
class Row;
class Column;

class Matrix {
public:
	Matrix() = default;

	class wrongMType : public std::exception {
	public:
		virtual const char* what() const override {
			return "Unexpected matrix type";
		}
	};
};

class Sparse : public Matrix {
public:
	Sparse() {
		std::cout << "Sparse Matrix created" << std::endl;
	}

	static Sparse* getSparse() {
		if (matrix == nullptr) {
			matrix = new Sparse();
		}
		return matrix;
	}

private:
	static Sparse* matrix;
};

class Row : public Matrix {
public:
	Row() {
		std::cout << "Row Matrix created" << std::endl;
	}

	static Row* getRow() {
		if (matrix == nullptr) {
			matrix = new Row();
		}
		return matrix;
	}

private:
	static Row* matrix;
};

class Column : public Matrix {
public:
	Column() {
		std::cout << "Column Matrix created" << std::endl;
	}

	static Column* getColumn() {
		if (matrix == nullptr) {
			matrix = new Column();
		}
		return matrix;
	}

private:
	static Column* matrix;
};

Column* Column::matrix = nullptr;
Row* Row::matrix = nullptr;
Sparse* Sparse::matrix = nullptr;

Matrix* getMatrix(MType t) {
	switch (t) {
	case MType::COLUMN:
		return Column::getColumn();
		break;
	case MType::ROW:
		return Row::getRow();
	case MType::SPARSE:
		return Sparse::getSparse();
		break;
	default:
		throw Matrix::wrongMType();
	}
}

int main() {
	try {
		Matrix* a = getMatrix(MType::COLUMN);
		Matrix* b = getMatrix(MType::ROW);
		Matrix* c = getMatrix(MType::SPARSE);
		Matrix* d = getMatrix(MType(42));
	}
	catch (Matrix::wrongMType& ex) {
		std::cout << "Caught exception: " << ex.what() << std::endl;
	}
	_getch();
}
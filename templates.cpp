#include <string>
#include <iostream>

// при вызове функции можно опустить тип выводимого 
// значения, т.к. он явно прописан в аргументах
template<typename T>
T MAX(T x, T y) {
	return x > y ? x : y;
}

// при вызове функции необходимо явно указывать,
// какого типа выводимое значение
template<typename T>
T getRandom() { return rand(); }

// i раз вывести в поток out
template<int i, typename T>
void repeat(std::ostream &os, T out) {
	for (int iter = 0; iter < i; iter++)
		os << out << " ";
	os << std::endl;
	return;
}

// можно по умолчанию задать значение
template<int I = 3, class T>
void repeat1(std::ostream &os, T out) {
	for (int iter = 0; iter < I; iter++)
		os << out << " ";
	os << std::endl;
	return;
}

// шаблонные классы
template<class T>
class TempClass {
	T data;
};

// когда надо для конкретного типа реализацию функции
// сделать немного по-другому
template<class T>
void print(std::ostream &os, T val) {
	os << val << std::endl;
};
// для строк - другая реализация
template<>
void print<std::string>(std::ostream &os, std::string val) {
	os << "string(" << val << ")" << std::endl;
};

int main() {
	int a = 5, b = 3;
	double x = 2.5, y = 2.6;
	float n = 3, m = 8.14;
	std::string str1 = "abc", str2 = "def";

	int c = MAX(a, b);
	double z = MAX(x, y);
	float k = MAX(n, m);
	std::string str = MAX(str1, str2);

	auto randint = getRandom<int>();
	auto randdouble = getRandom<double>();

	repeat<4>(std::cout, 42);
	repeat<3>(std::cout, "hello");

	repeat1(std::cout, "draste");
	// дальше код не будет компилироваться, так как
	// компилятор не присваивает тип Т к int
	// и пытается подставить в первый аргумент

	// repeat1<int>(std::cout, "mistake");

	TempClass<int> IntClass;
	TempClass<double> DoubleClass;

	return 0;
}

// шаблоны пишутся в файлах с расшин\рениями *.h, *.hpp и подключаются